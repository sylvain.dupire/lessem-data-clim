# Données climatiques et météorologiques au LESSEM

![INRAE](./ressource/img/Logo-INRAE.jpg?raw=true)
 &nbsp;&nbsp;&nbsp;
![LESSEM](./ressource/img/LESSEM_couleur.png?raw=true)  

&nbsp;  

Version : 1  
Contacts:  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sylvain DUPIRE (*sylvain.dupire@inrae.fr*)  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Hugues FRANCOIS (*hugues.francois@inrae.fr*)  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Isabelle BOULANGEAT (*isabelle.boulangeat@inrae.fr*)  

## Données disponibles sur infogeo¹


¹ : Il faut demander les droits d'accès à infogeo

&nbsp;  
## Autres données en libre accès
### France uniquement : 
* FYRECLIMATE : Réanalyse journalière sur la grille SAFRAN 8km x 8km de 1871 à 2012.  
 &nbsp;&nbsp;&nbsp;- précipitations : https://doi.org/10.5281/zenodo.4005573  
 &nbsp;&nbsp;&nbsp;- températures : https://doi.org/10.5281/zenodo.4006472


### Europe / Monde : 
* COPERNICUS CLIMATE CHANGE SERVICE : https://cds.climate.copernicus.eu/#!/home
* CHELSA CLIMATE : https://chelsa-climate.org/
* WORLDCLIM : https://www.worldclim.org/data/index.html



&nbsp;  
## Dossier Scripts 

Ce dossier regroupe des scripts pour extraire et mettre en forme des données climatiques et météorologiques.  

&nbsp;  
### Sous Python  

Il est assez facile de créer un environnement Python en utilisant miniconda : https://docs.conda.io/en/latest/miniconda.html.

Pour gérer les données climatiques et installer les librairies nécessaires, on peut créer un environnement spécifique par exemple appelé "pyclim" de la façon suivante en ligne de dommande:

```bash

conda create -n pyclim python=3.9 numpy scipy netCDF4 xarray pandas -c conda-forge
  
```  

Il faut ensuite se rendre dans le bon environnement en tapant :

```bash

conda activate pyclim
  
```  
 