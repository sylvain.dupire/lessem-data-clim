#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Script pour extraire des valeurs d'un NETCDF multilayer
@author: Sylvain DUPIRE, LESSEM, INRAE 2022
Pour disposer de l'environnement Python qui va bien il faut installer miniconda : https://docs.conda.io/en/latest/miniconda.html
Une fois l'installation terminée il faut créer un environnement  que l'on peut appelé pyclim avec la commande suivante :
conda create -n pyclim python=3.9 numpy netCDF4 xarray

Pour ajouter xarray a son conda
conda activate pyclim
conda install xarray -c conda-forge
"""
import os,sys
from datetime import datetime, timedelta
from calendar import monthrange
import numpy as np
from netCDF4 import Dataset,MFDataset
import matplotlib.pyplot as plt
import xarray as xr

### Nom du fichier netcf
metfile_path_start = '/media/sylvain/Ext_SDupire/0_Donnees/chelsa/CHELSA_EUR11_tasmin_day_'
years = ['2000','2001'] #liste des années à traiter au format texte

def plotraster(raster):
    plt.imshow(raster)
    plt.colorbar()
    plt.show()

def get_start_time(ncdf_dat):
    t=ncdf_dat.variables['time']
    t_split = t.units.split(' ')
    day_split = t_split[2].split('-')
    h_split = t_split[3].split(':')
    d0 = datetime(int(day_split[0]),
                  int(day_split[1]),
                  int(day_split[2]),
                  int(h_split[0]))
    return d0   

def get_tasmin_monthly(a,metfile_path_start):  
    #load file
    filepath = metfile_path_start +a+"_V1.1.nc"
    print(filepath)  
    ncdf_dat = Dataset(filepath)
    
    #get time vector
    t=ncdf_dat.variables['time']
    #get start date
    dstart = get_start_time(ncdf_dat)+timedelta(days=t[0].data.item())  
    #get end date
    dend = get_start_time(ncdf_dat) + timedelta(days=t[-1].data.item()) 
    #get timestep
    deltat = t[1].data.item()-t[0].data.item()
    dates = np.arange(np.datetime64(dstart), np.datetime64(dend+timedelta(days=1)), timedelta(days=deltat ))
    #get a list of months
    month_list = dates.astype('datetime64[M]').astype(int) % 12
    months=np.unique(month_list,return_index=True)
    varmet = ncdf_dat.variables['tasmin']
    #create monthly min raster
    MonthMinRast = np.zeros((len(months[0]),varmet.shape[1],varmet.shape[2]),dtype=np.int32)
    #on enregistre en multipliant par 100
    #loop to n-1 months
    prevind=0
    for i in months[0][:-1]:
        MonthMinRast[i]=np.minimum.reduce(varmet[prevind:months[1][i+1]])*100
        prevind=months[1][i+1]
    #add last month
    MonthMinRast[-1]=np.minimum.reduce(varmet[prevind:])*100
    return MonthMinRast

def get_tasmin_monthlyxarray(a,metfile_path_start):  
    #load file
    filepath = metfile_path_start +a+"_V1.1.nc"
    print(filepath)  
    ncdf_dat = xr.open_dataset(filepath)
    tmin = ncdf_dat.groupby('time.month').min()   
    return tmin

def create_out_dataset(years,metfile_path_start):
    filepath = metfile_path_start +years[0]+"_V1.1.nc"
    ncdf_dat = Dataset(filepath)
    varmet = ncdf_dat.variables['tasmin']
    Temp = np.zeros((len(years),12,varmet.shape[1],varmet.shape[2]),dtype=np.int32)
    return Temp
  
def saveglobalmin(metfile_path_start):
    tmin = xr.open_dataset(os.path.dirname(metfile_path_start)+"/Tmin_Year_Month.nc")
    globalmin = tmin.tasmin.min(dim='year')    
    filename = os.path.dirname(metfile_path_start)+"/globalTmin.nc"
    encoding = {'tasmin': {'zlib': True, 'complevel': 5}}
    globalmin.to_netcdf(filename,format="NETCDF4", encoding=encoding)

def saveglobalquant(metfile_path_start,Temp,quant):
    globalquant = np.float32(np.quantile(Temp, quant, 0)/100.)
    globalquant[globalquant==65535]=np.nan

    tmin = xr.open_dataset(os.path.dirname(metfile_path_start)+"/globalTmin.nc")
    newdata=tmin.copy()
    newdata['tasmin'].data=globalquant
    filename = os.path.dirname(metfile_path_start)+"/globalquant_"+str(quant)+"_Tmin.nc"
    encoding = {'tasmin': {'zlib': True, 'complevel': 5}}
    newdata.to_netcdf(filename,format="NETCDF4", encoding=encoding)
    
def save_allmonth(Temp,years,tmin,metfile_path_start):
    #copy tmin    
    newdata=tmin.copy()
    newdata = newdata.expand_dims(dim={"year":np.array(years)})
    Temp2 = np.float32(Temp/100.)
    Temp2[Temp2==65535]=np.nan
    newdata['tasmin'].data=Temp2
    filename = os.path.dirname(metfile_path_start)+"/Tmin_Year_Month.nc"
    encoding = {'tasmin': {'zlib': True, 'complevel': 5}}
    newdata.to_netcdf(filename,format="NETCDF4", encoding=encoding)
    
##############################################################################
### Compute over years
##############################################################################
Temp = create_out_dataset(years,metfile_path_start)

for i,a in enumerate(years):      
    tmin = get_tasmin_monthlyxarray(a,metfile_path_start)
    tmin['tasmin'].data[np.isnan(tmin['tasmin'].data)]=65535
    Temp[i] = np.int32(tmin['tasmin'].data*100)

###############################################################################
### save into numpy format in the same folder than chelsa files
###############################################################################
filename = os.path.dirname(metfile_path_start)+"/YearMonth_tmin.npz"
np.savez_compressed(filename,Tmin=Temp)
print('results saved as npz')

###############################################################################
### save into netcdf format in the same folder than chelsa files
###############################################################################
save_allmonth(Temp,years,tmin,metfile_path_start)
del tmin

###############################################################################
### Load npz
###############################################################################

# data = np.load(os.path.dirname(metfile_path_start)+"/YearMonth_tmin.npz")
# Temp = data["Tmin"]

###############################################################################
### save global min to netcdf
###############################################################################
saveglobalmin(metfile_path_start)
print('global Tmin results saved as netcdf')
###############################################################################
### save quantile to netcdf
###############################################################################
saveglobalquant(metfile_path_start,Temp,0.05)
print('global quantile '+str(0.05)+' results saved as netcdf')